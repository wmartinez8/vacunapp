﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Metodos
{
    public class SqlHelper
    {
        string connectionString = string.Empty;
        public SqlHelper()
        {
            connectionString = ConfigurationManager.ConnectionStrings["VacunaConnection"].ConnectionString;
        }


        //Metodo para el Select
        public DataTable executeQuery(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                DataTable dt = new DataTable();
                cmd.CommandType = commandType;
                if (parameters != null)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
        }

        public bool executeNonQuery(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            bool b = false;
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(commandText, conn))
            {
                conn.Open();
                cmd.CommandText = commandText;
                cmd.Parameters.AddRange(parameters);

                int result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    b = true;
                }
                else
                {
                    b = false;
                }
            }
            return b;

        }

    }
}
