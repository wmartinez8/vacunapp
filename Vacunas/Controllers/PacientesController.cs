﻿using Data.Entidades;
using Data.Metodos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vacunas.Models;

namespace Vacunas.Controllers
{
    public class PacientesController : Controller
    {
        //  string connectionString = ConfigurationManager.ConnectionStrings["VacunaConnection"].ConnectionString;
        SqlHelper helper = new SqlHelper();
        // GET: Vacunas
        public ActionResult Index()
        {

            return View();
        }


        public ActionResult EditarPaciente(int id)
        {
            var paciente = BuscaPaciente(id);
            return View(paciente);
        }

        public ActionResult CrearPaciente()
        {

            return View();
        }

        [HttpPost]
        public ActionResult CrearPaciente(PacienteDTO pacienteDTO)
        {
            var newNombres = pacienteDTO.Nombres;
            var newApellido1 = pacienteDTO.Apellido1;
            var newApellido2 = pacienteDTO.Apellido2;
            var newFechaNacimiento = pacienteDTO.FechaNacimiento;
            var newCedula = pacienteDTO.Cedula;
            var newTelefono = pacienteDTO.Telefono;
            var newDireccion = pacienteDTO.Direccion;
            var newSector = pacienteDTO.Sector;
            var newProvinciaId = pacienteDTO.ProvinciaId;
            var newArsId = pacienteDTO.ArsId;

            SqlParameter[] parameters = {
                new SqlParameter("@Nombres", newNombres),
                new SqlParameter("@Apellido1", newApellido1),
                new SqlParameter("@Apellido2", newApellido2),
                new SqlParameter("@FechaNacimiento", newFechaNacimiento),
                new SqlParameter("@Cedula", newCedula),
                new SqlParameter("@Telefono", newTelefono),
                new SqlParameter("@Direccion", newDireccion),
                new SqlParameter("@Sector", newSector),
                new SqlParameter("@ProvinciaId", newProvinciaId),
                new SqlParameter("@ArsId", newArsId),
            };
            var resultado = helper.executeNonQuery(@"exec addPaciente @Nombres, @Apellido1, @Apellido2, @FechaNacimiento,
                                                    @Cedula, @Telefono, @Direccion, @Sector, @ProvinciaId, @ArsId", CommandType.Text, parameters);

            return View("Index");
        }


        [HttpPost]
        public ActionResult EditarPaciente(PacienteDTO pacienteDTO)
        {
            var paciente = pacienteDTO;
            return View(paciente);
        }

        public JsonResult AllPacientes()
        {

            #region Metodo manual; se cambia para utilizarlo desde la capa Data
            //    List<PacienteDTO> lsPacientesDto = new List<PacienteDTO>();
            //using (SqlConnection conn = new SqlConnection(connectionString))
            //using (SqlCommand cmd = new SqlCommand("SELECT * FROM Pacientes", conn))
            //{
            //    DataTable dt = new DataTable();
            //    cmd.CommandType = CommandType.Text;
            //    SqlDataAdapter da = new SqlDataAdapter(cmd);
            //    da.Fill(dt);
            //    conn.Close();
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        PacienteDTO pacienteDTO = new PacienteDTO();
            //        pacienteDTO.PacienteId = int.Parse(dt.Rows[i]["PacienteId"].ToString());
            //        lsPacientesDto.Add(pacienteDTO);

            //    }
            //    return lsPacientesDto;
            //} 
            #endregion
            var resultado = helper.executeQuery("EXEC getPacientes",
                                                CommandType.Text, null);
            List<PacienteDTO> lsPacientes = new List<PacienteDTO>();
            for (int i = 0; i < resultado.Rows.Count; i++)
            {
                Paciente paciente = new Paciente();
                PacienteDTO pacienteDTO = new PacienteDTO();

                //Pacientes de la entidad en la capa DATA
                paciente.PacienteId = int.Parse(resultado.Rows[i]["PacienteId"].ToString());
                paciente.Nombres = resultado.Rows[i]["Nombres"].ToString();
                paciente.Apellido1 = resultado.Rows[i]["Apellido1"].ToString();
                paciente.Apellido2 = resultado.Rows[i]["Apellido2"].ToString();
                paciente.Cedula = resultado.Rows[i]["FechaNacimiento"].ToString();
                paciente.Cedula = resultado.Rows[i]["Cedula"].ToString();
                paciente.Telefono = resultado.Rows[i]["Telefono"].ToString();
                paciente.Direccion = resultado.Rows[i]["Direccion"].ToString();
                paciente.Sector = resultado.Rows[i]["Sector"].ToString();
                paciente.Provincia = resultado.Rows[i]["Pronvincia"].ToString();
                paciente.ARS = resultado.Rows[i]["ARS"].ToString();

                //Paciente en el modelo; asignandole el valor de data. Para evitar que el usuario interactue con DATA directamente
                pacienteDTO.PacienteId = paciente.PacienteId;
                pacienteDTO.Nombres = paciente.Nombres;
                pacienteDTO.Apellido1 = paciente.Apellido1;
                pacienteDTO.Apellido2 = paciente.Apellido2;
                pacienteDTO.FechaNacimiento = paciente.FechaNacimiento;
                pacienteDTO.Cedula = paciente.Cedula;
                pacienteDTO.Telefono = paciente.Telefono;
                pacienteDTO.Direccion = paciente.Direccion;
                pacienteDTO.Sector = paciente.Sector;
                pacienteDTO.Provincia = paciente.Provincia;
                pacienteDTO.ARS = paciente.ARS;

                lsPacientes.Add(pacienteDTO);
            }
            return Json(lsPacientes, JsonRequestBehavior.AllowGet);

            //  return lsPacientes;

        }

        public ActionResult addPaciente()
        {
            return View("Index");
        }

        public PacienteDTO BuscaPaciente(int id)
        {
            var resultado = helper.executeQuery("select pa.PacienteId, pa.Nombres, pa.Apellido1, pa.Apellido2, pa.Cedula, pa.Telefono," +
                "                               pa.Direccion, pa.Sector, prov.Nombre as Provincia, ars.Nombre as ARS  from pacientes pa " +
                "                               join Provincias prov on pa.ProvinciaId = prov.ProvinciaId join ars on pa.ArsId = ars.ArsId " +
                "                               WHERE pa.PacienteId = '" + id + "'", CommandType.Text, null);

            PacienteDTO pacienteDTO = new PacienteDTO();
            Paciente paciente = new Paciente
            {


                //Pacientes de la entidad en la capa DATA
                PacienteId = int.Parse(resultado.Rows[0]["PacienteId"].ToString()),
                Nombres = resultado.Rows[0]["Nombres"].ToString(),
                Apellido1 = resultado.Rows[0]["Apellido1"].ToString(),
                Apellido2 = resultado.Rows[0]["Apellido2"].ToString(),
                Cedula = resultado.Rows[0]["Cedula"].ToString(),
                Telefono = resultado.Rows[0]["Telefono"].ToString(),
                Direccion = resultado.Rows[0]["Direccion"].ToString(),
                Sector = resultado.Rows[0]["Sector"].ToString(),
                Provincia = resultado.Rows[0]["Provincia"].ToString(),
                ARS = resultado.Rows[0]["ARS"].ToString()
            };

            //Paciente en el modelo; asignandole el valor de data. Para evitar que el usuario interactue con DATA directamente
            pacienteDTO.PacienteId = paciente.PacienteId;
            pacienteDTO.Nombres = paciente.Nombres;
            pacienteDTO.Apellido1 = paciente.Apellido1;
            pacienteDTO.Apellido2 = paciente.Apellido2;
            pacienteDTO.Cedula = paciente.Cedula;
            pacienteDTO.Telefono = paciente.Telefono;
            pacienteDTO.Direccion = paciente.Direccion;
            pacienteDTO.Sector = paciente.Sector;
            pacienteDTO.Provincia = paciente.Provincia;
            pacienteDTO.ARS = paciente.ARS;



            return pacienteDTO;
        }

        public bool modificarPaciente(int id, PacienteDTO pacienteDTO)
        {
            pacienteDTO.PacienteId = id;
            pacienteDTO.Nombres = "Miguelito";
            pacienteDTO.Apellido1 = "Cueva";
            pacienteDTO.Apellido2 = "Otro apellido";

            SqlParameter[] sqlParameters = {
                new SqlParameter("@PacienteId",pacienteDTO.PacienteId),
                new SqlParameter("@Nombres",pacienteDTO.Nombres),
                new SqlParameter("@Apellido1",pacienteDTO.Apellido1),
                new SqlParameter("@Apellido2",pacienteDTO.Apellido2),
            };

            var resultado = helper.executeNonQuery(@"UPDATE PACIENTES SET Nombres = @Nombres, Apellido1 =  @Apellido1, Apellido2 = @Apellido2, Cedula = @Cedula where PacienteId = @PacienteId",
                                                    CommandType.Text, sqlParameters);

            return resultado;
        }

        // la funcion de este metodo se esta repitiendo; se cambio el return de Allpacientes y listo.
        //public JsonResult getPacientes()
        //{
        //    var pacientesJson = AllPacientes();
        //    return Json(pacientesJson, JsonRequestBehavior.AllowGet);
        //}

    }
}