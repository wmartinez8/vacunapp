﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vacunas.Models
{
    public class PacienteDTO
    {

        public int PacienteId { get; set; }
        public string Nombres { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Cedula { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Sector { get; set; }
        public int ProvinciaId { get; set; }
        public int ArsId { get; set; }

        public virtual string Provincia { get; set; }
        public virtual string ARS { get; set; }


    }
}